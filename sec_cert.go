package saferest

import "io"

// SectionCert identifies the cert config section
const SectionCert = "cert"

// CertConfig contains the certificate configuration parameters
type CertConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// Path to the Root CA cert bundle (PEM format)
	RootFile string `json:"rootFile" xml:"root_file" yaml:"rootFile"`
	// Path to the server's certificate and private key (PEM format)
	CertFile string `json:"certFile" xml:"cert_file" yaml:"certFile"`
	KeyFile  string `json:"keyFile" xml:"key_file" yaml:"keyFile"`
	// Domain name and key length for self-signed cert
	CertDomain string `json:"certDomain" xml:"cert_domain" yaml:"certDomain"`
	KeyBits    int    `json:"keyBits" xml:"key_bits" yaml:"keyBits"`
}

// NewCertConfig adds the CertConfig to a ConfigMa ager
func newCertConfig(parser Parser, input io.Reader) Config {
	// Set sensible default values for numeric parameters
	config := CertConfig{
		Version:    1,
		RootFile:   "",
		CertFile:   "",
		KeyFile:    "",
		CertDomain: "localhost",
		KeyBits:    2048,
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetCert returns certificate config
func GetCert(manager Sectioner) CertConfig {
	return manager.Section(SectionCert).(CertConfig)
}
