package saferest

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Server is the Self-signing enabled REST server
type Server struct {
	lruCache
	// Server object
	HTTPSrv http.Server
	// Certificate and Key Path
	KeyFile  string
	CertFile string
	// Config manager
	manager ConfigManager
	// Etag cache
}

// NewConfigManager creates an empty service manager. If secured, adds support for JWT.
func NewConfigManager(secured bool) ConfigManager {
	manager := configManager{
		mutex:      sync.RWMutex{},
		configData: make(map[string]*configData),
	}
	// Adds default sections needed at least for server bootstrap.
	sections := []ConfigDescriptor{
		{Section: SectionServer, Parser: YAMLParser(SectionServer), Factory: newServerConfig},
		{Section: SectionCert, Parser: YAMLParser(SectionCert), Factory: newCertConfig},
	}
	if secured {
		sections = append(sections, ConfigDescriptor{Section: SectionToken, Parser: YAMLParser(SectionToken), Factory: newTokenConfig})
	}
	for _, d := range sections {
		manager.AddFactory(d)
	}
	return &manager
}

// NewServer creates a new Server object
func NewServer(manager ConfigManager, configDir string, debug bool) (*Server, error) {
	// Load config
	if err := manager.LoadFrom(configDir, false); err != nil {
		log.Panic("Error loading config files, ", err)
	}
	// Create server
	certConfig := GetCert(manager)
	serverConfig := GetServer(manager)
	router := mux.NewRouter()
	server := &Server{
		HTTPSrv: http.Server{
			Addr:           ":" + strconv.Itoa(serverConfig.Port),
			MaxHeaderBytes: serverConfig.MaxHeaderSize << 10, // KBytes to bytes
			ReadTimeout:    time.Duration(serverConfig.ReadTimeout) * time.Second,
			WriteTimeout:   time.Duration(serverConfig.ReadTimeout) * time.Second,
			Handler:        router.StrictSlash(true),
		},
		manager: manager,
	}
	// Prepare cache middleware settings
	if serverConfig.EtagCache > 0 {
		server.lruCache.init(serverConfig.EtagCache)
	}
	// Route debug calls and metrics to profiler (requires -pprof build tag, too)
	router.Handle("/metrics", promhttp.Handler())
	if debug { // more advanced, real time profiling
		router.PathPrefix("/debug").Handler(http.DefaultServeMux)
	}
	// If HTTPS enable, retrieve or generate cert
	if serverConfig.HTTPS {
		httpsrv := &server.HTTPSrv
		selfsign := true
		// If both the key and certificate paths are configured,
		// then do not self-sign the cert but load them instead
		if certConfig.CertFile != "" && certConfig.KeyFile != "" {
			selfsign = false
			server.CertFile = certConfig.CertFile
			server.KeyFile = certConfig.KeyFile
		}
		if err := enableTLS(httpsrv, certConfig, selfsign); err != nil {
			return nil, err
		}
	}
	return server, nil
}

// Debug prints the contents of all config sections
func (server *Server) Debug() {
	sections := server.manager.Sections()
	for _, section := range sections {
		log.Print("Config Section ", section)
		config := server.manager.Section(section)
		bin, _ := json.Marshal(config)
		log.Print(string(bin))
	}
}

// helper function for not found routes
func notFound(w http.ResponseWriter, r *http.Request) {
	err := errors.Errorf("Path %s for method %s not found", r.URL.Path, r.Method)
	Fail(http.StatusNotFound, w, err)
}

// Adds routes to the server
func (server *Server) addRoutes(routes []Route) {
	router, ok := server.HTTPSrv.Handler.(*mux.Router)
	if !ok {
		log.Panic("Could not cast Handler to mux.Router")
	}
	serverConfig := GetServer(server.manager)
	params := &routeParams{
		manager: server.manager,
		cache:   server,
		router:  router,
		prefix:  serverConfig.Prefix,
	}
	// Add explicit routes
	for _, route := range routes {
		addRoute(params, route)
	}
	// Add default route, if any
	if serverConfig.Redirect != "" {
		defaultRoute := serverConfig.Prefix + serverConfig.Redirect
		redirect := func(w http.ResponseWriter, r *http.Request) {
			http.Redirect(w, r, defaultRoute, http.StatusFound)
		}
		route := Route{
			Audience: "",
			Method:   "GET",
			Pattern:  "/",
			Private:  false,
			Compress: false,
			Cache:    false,
			Handler:  redirect,
		}
		addRoute(params, route)
	}
	// Add Static folder config, if any
	if serverConfig.Static != "" {
		static := http.StripPrefix("/static/", http.FileServer(http.Dir(serverConfig.Static)))
		router.PathPrefix("/static/").Handler(static)
	}
	// Add not found handler
	router.NotFoundHandler = http.HandlerFunc(notFound)
}

// Start the server
func (server *Server) Start(routes []Route) error {
	server.addRoutes(routes)
	// Set up signal management to stop the server on
	// SIGINT, SIGTERM. Commented out - does not work in Windows.
	/*
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
		go func(server *Server) {
			sig := <-sigs
			log.Print("Received signal ", sig, ", now shutting down the server...")
			server.HTTPSrv.Shutdown(nil)
		}(server)
	*/
	// Don't know why, recently I stopped seeing the date in error logs...
	log.SetFlags(log.LstdFlags)
	// Run the server
	serverConfig := GetServer(server.manager)
	if serverConfig.HTTPS {
		return server.HTTPSrv.ListenAndServeTLS(server.CertFile, server.KeyFile)
	}
	return server.HTTPSrv.ListenAndServe()
}
