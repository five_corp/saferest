package saferest

import (
	"encoding/json"
	"encoding/xml"
	"io"
	"io/ioutil"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

// Parser to encode and decode Config objects
type Parser interface {
	Encode(config interface{}) ([]byte, error)
	Decode(input io.Reader, config interface{}) error
	String() string
	FileName() string
}

type (
	// JSONParser implements Parser interface for JSON config files
	JSONParser string
	// XMLParser implements Parser interface for XML config files
	XMLParser string
	// YAMLParser implements Parser interface for YAML config files
	YAMLParser string
)

// Config interface
type Config interface{}

// Encode the config in JSON format
func (JSONParser) Encode(config interface{}) ([]byte, error) {
	data, err := json.MarshalIndent(config, "", "  ")
	return data, errors.Wrap(err, "Failed to encode config")
}

// Decode the config from JSON reader
func (JSONParser) Decode(input io.Reader, config interface{}) error {
	return errors.Wrap(json.NewDecoder(input).Decode(config), "Failed to decode config")
}

// String returns the name of the parser
func (p JSONParser) String() string {
	return string(p)
}

// FileName returns the full file name with extension
func (p JSONParser) FileName() string {
	return string(p) + ".json"
}

// Encode the config in XML format
func (XMLParser) Encode(config interface{}) ([]byte, error) {
	result, err := xml.MarshalIndent(config, "", "  ")
	if err == nil {
		result = append([]byte(xml.Header), result...)
	}
	return result, errors.Wrap(err, "Failed to encode config")
}

// Decode the config from XML reader
func (XMLParser) Decode(input io.Reader, config interface{}) error {
	return errors.Wrap(xml.NewDecoder(input).Decode(config), "Failed to decode config")
}

// String returns the name of the parser
func (p XMLParser) String() string {
	return string(p)
}

// FileName returns the full file name with extension
func (p XMLParser) FileName() string {
	return string(p) + ".xml"
}

// Encode the config in YAML format
func (YAMLParser) Encode(config interface{}) ([]byte, error) {
	data, err := yaml.Marshal(config)
	return data, errors.Wrap(err, "Failed to encode config")
}

// Decode the config from YAML reader
func (YAMLParser) Decode(input io.Reader, config interface{}) error {
	data, err := ioutil.ReadAll(input)
	if err != nil {
		return errors.Wrap(err, "Failed to decode config")
	}
	if data, err = yamlToJSON(data); err != nil {
		return err
	}
	return errors.Wrap(json.Unmarshal(data, config), "Failed to unmarshal config")
}

// String returns the name of the parser
func (p YAMLParser) String() string {
	return string(p)
}

// FileName returns the full file name with extension
func (p YAMLParser) FileName() string {
	return string(p) + ".yaml"
}

// Helper function to convert YAML to JSON
// See http://stackoverflow.com/questions/40737122/convert-yaml-to-json-without-struct-golang
func yamlToJSON(input []byte) ([]byte, error) {
	var body interface{}
	if err := yaml.Unmarshal(input, &body); err != nil {
		return nil, errors.Wrap(err, "Failed to unmarshal yaml content")
	}
	output, err := json.Marshal(convert(body))
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal yaml to json")
	}
	return output, nil
}

func convert(i interface{}) interface{} {
	switch x := i.(type) {
	case map[interface{}]interface{}:
		m2 := map[string]interface{}{}
		for k, v := range x {
			m2[k.(string)] = convert(v)
		}
		return m2
	case []interface{}:
		for i, v := range x {
			x[i] = convert(v)
		}
	}
	return i
}
