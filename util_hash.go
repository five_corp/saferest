package saferest

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"encoding/hex"
	"hash"
	"io"
	"net/http"

	"github.com/pkg/errors"
	"golang.org/x/crypto/sha3"
)

// CryptoOptions returns the crypto options used for signing
func CryptoOptions() crypto.SignerOpts {
	return &rsa.PSSOptions{Hash: crypto.SHA3_512}
}

// HashReader computes the hash of the data as it reads it
type hashReader struct {
	hash.Hash
	io.Reader
}

// SignReader hashes and signs the data it reads
type SignReader struct {
	hashReader
	signer crypto.Signer
}

// hashWriter computes the hash of the data as it writes it
type hashWriter struct {
	hash.Hash
	http.ResponseWriter
}

// NewReader returns a new hashing reader
func newReader(reader io.Reader) hashReader {
	return hashReader{
		Hash:   sha3.New512(),
		Reader: reader,
	}
}

// NewSignReader returns a new SignReader
func NewSignReader(reader io.Reader, signer crypto.Signer) SignReader {
	return SignReader{
		hashReader: newReader(reader),
		signer:     signer,
	}
}

// newWriter returns a new HashWriter
func newWriter(writer http.ResponseWriter) hashWriter {
	h := hashWriter{
		Hash:           sha3.New512(),
		ResponseWriter: writer,
	}
	// Make sure hashWriter is an http.Flusher
	_ = http.Flusher(h)
	return h
}

// Read implements io.Reader
func (h hashReader) Read(p []byte) (n int, err error) {
	n, err = h.Reader.Read(p)
	if n > 0 {
		h.Write(p[:n])
	}
	// IMPORTANT! Do not mask errors with Wrap().
	// Library functions may be waiting for io.EOF.
	return n, err
}

// Close implements io.Closer
func (h hashReader) Close() error {
	if closer, ok := h.Reader.(io.Closer); ok {
		return closer.Close()
	}
	return nil
}

// Sign data read through the reader
func (h SignReader) Sign() (string, error) {
	s, err := h.signer.Sign(rand.Reader, h.Sum(nil), CryptoOptions())
	if err != nil {
		return "", errors.Wrap(err, "Failed to sign stream")
	}
	return hex.EncodeToString(s), nil
}

// Write implements io.Writer
func (w hashWriter) Write(p []byte) (n int, err error) {
	// Write to the internal writer
	n, err = w.ResponseWriter.Write(p)
	// Add to the hash whatever has been written
	if n > 0 {
		w.Hash.Write(p[:n])
	}
	// IMPORTANT! Do not mask errors with Wrap().
	// Library functions may be waiting for io.EOF.
	return n, err
}

// Flush implements http.Flusher
func (w hashWriter) Flush() {
	if flusher, ok := w.ResponseWriter.(http.Flusher); ok {
		flusher.Flush()
	}
}
