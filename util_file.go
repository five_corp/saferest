package saferest

import (
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
)

// PathElems splits the path and returns all elements in the path which are not empty
func PathElems(path string) []string {
	items := strings.Split(filepath.ToSlash(path), "/")
	filtered := make([]string, 0, len(items))
	for _, item := range items {
		if item != "" {
			filtered = append(filtered, item)
		}
	}
	return filtered
}

// IsDir checks if there is a dir at the given path
func IsDir(name string) (bool, error) {
	// Find if there is a directory
	info, err := os.Stat(name)
	if err != nil {
		return false, errors.Wrapf(err, "Failed to stat path %s", name)
	}
	if !info.IsDir() {
		return false, errors.Errorf("%s is not a directory", name)
	}
	return true, nil
}

// IsDirEmpty checks if a directory is empty.
// See http://stackoverflow.com/questions/30697324/how-to-check-if-directory-on-path-is-empty
func IsDirEmpty(name string) (bool, error) {
	// Find if there is a directory
	dir, err := IsDir(name)
	if err != nil || !dir {
		return false, err
	}
	handle, err := os.Open(name)
	if err != nil {
		return false, errors.Wrapf(err, "Failed to open file %s", name)
	}
	defer handle.Close()
	_, err = handle.Readdirnames(1) // Or f.Readdir(1)
	if errors.Cause(err) == io.EOF {
		return true, nil
	}
	// Either not empty or error, suits both cases
	return false, errors.Wrapf(err, "Failed to read contents of dir %s", name)
}

// EnsureDir makes sure the given path is a folder, creates it otherwise
func EnsureDir(folder string, truncate bool) error {
	// Get path info
	info, err := os.Stat(folder)
	if err != nil {
		// If not exists, create dir
		if os.IsNotExist(errors.Cause(err)) {
			return errors.Wrapf(os.MkdirAll(folder, os.ModePerm), "Failed to create folder %s", folder)
		}
		// Else, return error
		return errors.Wrapf(err, "Failed to stat folder %s", folder)
	}
	// If !isDir, and can't truncate, it is an error.
	if !info.IsDir() {
		if !truncate {
			return errors.Errorf("Path '%s' is not a directory", folder)
		}
	}
	// If IsDir and !truncate, then everything is ok, return.
	if !truncate {
		return nil
	}
	// Otherwise, remove it and create a new dir.
	if err := os.RemoveAll(folder); err != nil {
		return errors.Wrapf(err, "Failed to remove folder %s", folder)
	}
	return errors.Wrapf(os.MkdirAll(folder, os.ModePerm), "Failed to create folder %s", folder)
}

// VerifyFunc is a Verifier with embedded signature
type VerifyFunc func(hash []byte) error

// OverwriteFile safely overwrites a file.
func OverwriteFile(filename string, source io.Reader, vf VerifyFunc) error {
	// Write to a temporary output file
	// The temp file must be in the same filesystem as the
	// final file, so it can be renamed succesfully.
	tempName := filename + ".tmp"
	var hasher hashReader
	if vf != nil {
		hasher = newReader(source)
		source = hasher
	}
	if err := copyFile(tempName, source, 0644); err != nil {
		return err
	}
	if vf != nil {
		if err := vf(hasher.Sum(nil)); err != nil {
			return errors.Wrapf(err, "Failed to hash file %s", filename)
		}
	}
	// Rename the file and overwrite the current one
	if err := os.Rename(tempName, filename); err != nil {
		// clean the garbage file
		os.Remove(tempName)
		return errors.Wrapf(err, "Failed to rename file %s to %s", tempName, filename)
	}
	return nil
}

// Writes a file and closes it before returning. Useful for renaming later.
func copyFile(destPath string, src io.Reader, mode int) error {
	dest, err := os.OpenFile(destPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, os.FileMode(mode))
	if err == nil {
		defer dest.Close()
		// Perform the copy from input to output
		_, err = io.Copy(dest, src)
	}
	return errors.Wrapf(err, "Failed to copy file %s", destPath)
}

// ParseFile reads an struct from a file
func ParseFile(dir string, parser Parser, data interface{}) error {
	// Open the queries file from the "requested" folder
	fullPath := path.Join(dir, parser.FileName())
	input, err := os.Open(fullPath)
	if err != nil {
		return errors.Wrapf(err, "Failed to open file %s", fullPath)
	}
	defer input.Close()
	// Decode the config
	if err = parser.Decode(input, data); err != nil {
		return err
	}
	return nil
}
