package saferest

import "io"

// SectionServer identifies the server congif section
const SectionServer = "server"

// ServerConfig contains the server configuration parameters
type ServerConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// Run using HTTPS
	HTTPS bool `json:"https" xml:"https" yaml:"https"`
	// Port to listen at
	Port int `json:"port" xml:"port" yaml:"port"`
	// Read and Write HTTP timeouts (seconds)
	ReadTimeout  int `json:"readTimeout" xml:"read_timeout" yaml:"readTimeout"`
	WriteTimeout int `json:"writeTimeout" xml:"write_timeout" yaml:"writeTimeout"`
	// Maximum header size (KBytes).
	MaxHeaderSize int `json:"maxHeaderSize" xml:"max_header_size" yaml:"maxHeaderSize"`
	// Maximum body size (MBytes).
	MaxBodySize int `json:"maxBodySize" xml:"max_body_size" yaml:"maxBodySize"`
	// Maximum memory allocated for multipart file upload (MBytes)
	MaxFileMemory int `json:"maxFileMemory" xml:"max_file_memory" yaml:"maxFileMemory"`
	// Maximum etag cache size (units)
	EtagCache int `json:"etagCache" xml:"etag_cache" yaml:"etagCache"`
	// Maximum Cache Time (seconds)
	MaxAge int `json:"maxAge" xml:"max_age" yaml:"maxAge"`
	// API URL Prefix
	Prefix string `json:"prefix" xml:"prefix" yaml:"prefix"`
	// Redirect "/" location to this endpoint
	Redirect string `json:"redirect" xml:"redirect" yaml:"redirect"`
	// Static files folder
	Static string `json:"static" xml:"static" yaml:"static"`
}

// NewServerConfig adds the CertConfig to a ConfigMa ager
func newServerConfig(parser Parser, input io.Reader) Config {
	// Set sensible default values for numeric parameters
	config := ServerConfig{
		Version: 1,
		HTTPS:   false,
		Port:    3000,
		// Need large timeouts to upload large files.
		// I would like to extend the timeout just for
		// file posts, but it is not possible yet.
		// (see: https://github.com/golang/go/issues/16100)
		ReadTimeout:   300,            // seconds
		WriteTimeout:  300,            // seconds
		MaxHeaderSize: 4,              // 4 KBytes
		MaxBodySize:   32,             // 32 MBytes
		MaxFileMemory: 32,             // 32 MBytes
		EtagCache:     8192,           // 8192 tags
		MaxAge:        24 * 30 * 3600, // 1 month seconds
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetServer returns Server Config
func GetServer(manager Sectioner) ServerConfig {
	return manager.Section(SectionServer).(ServerConfig)
}
