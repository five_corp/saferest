package saferest

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"reflect"
	"strings"
	"sync"

	"github.com/pkg/errors"
)

// ConfigFactory is a factory function for Config objects
type ConfigFactory func(parser Parser, input io.Reader) Config

// ConfigDescriptor contains all data to configure a secvtion
type ConfigDescriptor struct {
	Section string
	Parser  Parser
	Factory ConfigFactory
}

// Sectioner is just the interface for the bare minimum Section management
type Sectioner interface {
	Section(section string) Config
	Parser(section string) Parser
	Has(section string) (Config, bool)
}

// SectionSaver is an interface that adds saving and retrieving sections
type SectionSaver interface {
	Sectioner
	Save(section string, config Config) error
}

// ConfigManager is the file manager and saver
type ConfigManager interface {
	// Base interfaces the ConfigManager implements
	SectionSaver
	// Global funcs: add factory, load and enumerate sections
	AddFactory(descriptor ConfigDescriptor)
	// LoadFrom reads data from the given config dir
	LoadFrom(configDir string, skipErrors bool) error
	Sections() []string
	// Replace a config without saving it
	Replace(section string, config Config)
}

// Data about a public field in a config struct
type fieldDescriptor struct {
	// command line flagKey, "section:json_name"
	flagKey string
	// True if the field is required at installation time
	mandatory bool
	// Pointer to value value read from the config file or the config's constructor
	value reflect.Value
}

// FlagData contains information on config fields that can be turned into command-line flags
type flagData struct {
	config reflect.Value
	fields map[string]fieldDescriptor
}

// Information about a config section
type configData struct {
	factory ConfigFactory
	parser  Parser
	config  Config
	flags   flagData
}

// Service manager
type configManager struct {
	configDir  string
	configData map[string]*configData
	mutex      sync.RWMutex
}

// Registers a configuration section.
func (m *configManager) AddFactory(descriptor ConfigDescriptor) {
	// Build an empty object to read its properties and set cli flags
	flags := readFields(descriptor.Section, descriptor.Factory(descriptor.Parser, nil))
	flags.addFlags(descriptor.Section)
	// And store the factory for reading from a file later
	m.configData[descriptor.Section] = &configData{
		factory: descriptor.Factory,
		config:  nil,
		parser:  descriptor.Parser,
		flags:   flags,
	}
}

// Creates a new map of Config field name -> FlagValue item
func readFields(section string, config Config) flagData {
	// I get a seferest.Config interface, which is a Config struct.
	// No longer do I need to dereference it.
	configItem := reflect.ValueOf(config)
	configType := configItem.Type()
	flags := flagData{
		config: reflect.New(configType).Elem(),
		fields: make(map[string]fieldDescriptor),
	}
	// Me quedo con los campos que tienen un tag json != "-"
	for i := 0; i < configItem.NumField(); i++ {
		field := configType.Field(i)
		jsonTag := field.Tag.Get("json")
		if field.Anonymous || jsonTag == "" || jsonTag == "-" {
			continue
		}
		// El tag puede llevar ",omitempty", me quedo con la primera parte
		jsonTag = strings.TrimSpace(strings.Split(jsonTag, ",")[0])
		if jsonTag == "omitempty" {
			continue
		}
		instTag := field.Tag.Get("flags")
		mandatory := false
		if instTag == "mandatory" {
			mandatory = true
		}
		if instTag == "skip" {
			continue
		}
		// Get the current value of the field
		flagKey, cloned := "", flags.config.Field(i)
		cloned.Set(configItem.Field(i))
		if cloned.CanAddr() {
			flagKey = section + ":" + jsonTag
		}
		val := fieldDescriptor{
			flagKey:   flagKey,
			mandatory: mandatory,
			value:     cloned,
		}
		flags.fields[field.Name] = val
	}
	return flags
}

// AddFlags set the command line flags
func (f flagData) addFlags(section string) {
	for _, flagValue := range f.fields {
		flagKey := flagValue.flagKey
		if flagKey == "" {
			continue
		}
		original, addr := flagValue.value, flagValue.value.Addr()
		usage := fmt.Sprintf("Parameters %s of section %s", flagKey, section)
		switch original.Kind() {
		case reflect.String:
			flag.StringVar(addr.Interface().(*string), flagKey, original.Interface().(string), usage)
		case reflect.Float64:
			flag.Float64Var(addr.Interface().(*float64), flagKey, original.Interface().(float64), usage)
		case reflect.Int:
			flag.IntVar(addr.Interface().(*int), flagKey, original.Interface().(int), usage)
		case reflect.Uint:
			flag.UintVar(addr.Interface().(*uint), flagKey, original.Interface().(uint), usage)
		case reflect.Int64:
			flag.Int64Var(addr.Interface().(*int64), flagKey, original.Interface().(int64), usage)
		case reflect.Uint64:
			flag.Uint64Var(addr.Interface().(*uint64), flagKey, original.Interface().(uint64), usage)
		case reflect.Bool:
			flag.BoolVar(addr.Interface().(*bool), flagKey, original.Interface().(bool), usage)
		}
	}
}

// LoadFrom reads the registered sections
func (m *configManager) LoadFrom(configDir string, skipErrors bool) error {
	// Get flags that have been set
	visited := make(map[string]bool)
	flag.Visit(func(f *flag.Flag) {
		visited[f.Name] = true
	})
	m.configDir = configDir
	for section, data := range m.configData {
		for _, flagVal := range data.flags.fields {
			if flagVal.mandatory && !visited[flagVal.flagKey] {
				return errors.Errorf("Command line parameter %s is required", flagVal.flagKey)
			}
		}
		log.Print("Loading config section ", section)
		if err := m.refresh(section); err != nil {
			if !skipErrors {
				return err
			}
			// data.update below will panic if data.config is nil
			// So, at least assign a default config.
			data.config = data.factory(data.parser, nil)
		}
		data.update(section, visited)
		log.Print("Config section ", section, " successfully loaded")
	}
	return nil
}

// Refresh: re-reads a config file
// If dryRun == true, do not really attempt to load the files.
func (m *configManager) refresh(section string) error {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	data, ok := m.configData[section]
	if !ok {
		log.Panic("Failed to refresh inexistent section")
	}
	parser := data.parser
	if parser.String() == "" {
		return errors.New("Config File not specified")
	}
	fullPath := path.Join(m.configDir, parser.FileName())
	input, err := os.Open(fullPath)
	if err != nil {
		return errors.Wrapf(err, "Fail to open config file %s", fullPath)
	}
	defer input.Close()
	data.config = data.factory(parser, input)
	return nil
}

func (data *configData) update(section string, nonDefault map[string]bool) {
	fileData := readFields(section, data.config)
	updated := false
	for name, fileFlag := range fileData.fields {
		original := fileFlag.value
		// command line arguments take precedence over current values
		if fileFlag.flagKey != "" && nonDefault[fileFlag.flagKey] && original.CanSet() {
			original.Set(data.flags.fields[name].value)
			updated = true
		}
	}
	if updated {
		data.config = fileData.config.Interface().(Config)
	}
}

// Return a list of sections
func (m *configManager) Sections() []string {
	m.mutex.RLock()
	keys := make([]string, 0, len(m.configData))
	for section := range m.configData {
		keys = append(keys, section)
	}
	m.mutex.RUnlock()
	return keys
}

// Save: stores the config file. Safe to be used from distinct goroutines.
func (m *configManager) Save(section string, config Config) error {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	data, ok := m.configData[section]
	if !ok || data.config == nil {
		return errors.New("Config section " + section + " not found")
	}
	data.config = config
	parser := data.parser
	if parser.String() == "" {
		return errors.New("Output file not specified")
	}
	// Marshal first so it won't fail later when we have already
	// started to mess with files.
	buf, err := parser.Encode(data.config)
	if err != nil {
		return err
	}
	// Write safely.
	fullPath := path.Join(m.configDir, parser.FileName())
	return OverwriteFile(fullPath, bytes.NewReader(buf), nil)
}

// Gets a section of the config
func (m *configManager) Section(section string) Config {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	if data, ok := m.configData[section]; ok {
		if data.config != nil {
			return data.config
		}
	}
	panic("Missing section " + section)
}

// Has returns a section of the config
func (m *configManager) Has(section string) (Config, bool) {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	if data, ok := m.configData[section]; ok {
		if data.config != nil {
			return data.config, true
		}
	}
	return nil, false
}

// Gets a section of the config
func (m *configManager) Parser(section string) Parser {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	if data, ok := m.configData[section]; ok {
		return data.parser
	}
	panic("Missing section " + section)
}

// Gets a section of the config
func (m *configManager) Replace(section string, config Config) {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	data, ok := m.configData[section]
	if !ok {
		panic("Missing section " + section)
	}
	data.config = config
}
