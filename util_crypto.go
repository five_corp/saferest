package saferest

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"encoding/asn1"
	"encoding/hex"
	"encoding/pem"
	"io/ioutil"
	"log"
	"math/big"
	"os"

	"github.com/pkg/errors"
)

const (
	// InvalidData is returned when we cannot read a block of PEM data
	InvalidData = "Could not decode PEM File"
	// NoCertFound is returned when we cannot find a certificate in the PEM file
	NoCertFound = "No certificate found in file"
	// NoKeyFound is returned when we cannot find a key in the PEM file
	NoKeyFound = "No private key found in file"
	// InvalidECSignature returned when the ECDSA signature does not match
	InvalidECSignature = "The ECDSA Signature does not match"
	// UnknownKeyAlgorithm returned when the Public Key Algorithm is not recognized
	UnknownKeyAlgorithm = "Unknown public key algorithm"
	// PublicKeyMismatch returned when the algorithm in the Cert does not match the algorithm in the Public Key
	PublicKeyMismatch = "Mismatched algorithm between Cert and Public Key"
	// InvalidSignature returned when no verifier could match the signature
	InvalidSignature = "None of the verifiers matched the signature"
)

// SignerFrom returns a crypto.Signer from a given PEM key file
func SignerFrom(file, password string) (crypto.Signer, error) {
	input, err := newPemReader(file, password, false)
	if err != nil {
		return nil, err
	}
	for input.Next() {
		block := input.Get()
		switch block.Type {
		case "RSA PRIVATE KEY":
			return x509.ParsePKCS1PrivateKey(block.Bytes)
		case "EC PRIVATE KEY":
			return x509.ParseECPrivateKey(block.Bytes)
		}
	}
	if err := input.Error(); err != nil {
		return nil, err
	}
	return nil, errors.New(NoKeyFound)
}

// Verifier checks a signature against a public key
type Verifier interface {
	// Verify checks that the signature matches the hash of the file
	Verify(hash []byte, signature string) error
}

// VerifierFrom returns a Verifier from a given list of PEM certificates.
func VerifierFrom(pemFiles []string) (Verifier, error) {
	if pemFiles == nil || len(pemFiles) == 0 {
		return nil, errors.New("No pemFiles provided to build verifier from")
	}
	verifiers := make([]Verifier, 0, len(pemFiles))
	for _, pemFile := range pemFiles {
		input, err := newPemReader(pemFile, "", false)
		if err != nil {
			// Fail-open behaviour: if we cannot read a file, go for the next
			log.Print("ERROR: Cannot read file ", pemFile, ", skipping. Error: ", err)
			continue
			//return nil, Wrap(err)
		}
		if input.Next() && input.Get().Type == "CERTIFICATE" {
			cert, err := x509.ParseCertificate(input.Get().Bytes)
			if err != nil {
				// Fail-open behaviour: if we cannot read a file, go for the next
				log.Print("ERROR: Failed to read certificate from ", pemFile, ", error: ", err)
				//return nil, Wrap(err)
			}
			switch cert.PublicKeyAlgorithm {
			case x509.RSA:
				if pkey, ok := cert.PublicKey.(*rsa.PublicKey); !ok {
					// Fail-open behaviour: if we cannot read a file, go for the next
					log.Print("ERROR: Expected RSA public key in ", pemFile)
					//return nil, Wrap(PublicKeyMismatch)
				} else {
					verifiers = append(verifiers, rsaVerifier{pkey: pkey})
				}
			case x509.ECDSA:
				if pkey, ok := cert.PublicKey.(*ecdsa.PublicKey); !ok {
					// Fail-open behaviour: if we cannot read a file, go for the next
					log.Print("ERROR: Expected ECDSA public key in ", pemFile)
					//return nil, Wrap(PublicKeyMismatch)
				} else {
					verifiers = append(verifiers, ecVerifier{pkey: pkey})
				}
			default:
				// Fail-open behaviour: if we cannot read a file, go for the next
				log.Print("ERROR: Unknown key algorithm in certificate file ", pemFile)
				//return nil, Wrap(UnknownKeyAlgorithm)
			}
		}
		if err := input.Error(); err != nil {
			// Fail-open behaviour: if we cannot read a file, go for the next
			log.Print("ERROR: Failed to decode block from ", pemFile, ": ", err)
			//return nil, Wrap(err)
		}
	}
	if len(verifiers) == 0 {
		return nil, errors.Errorf("No valid key could be found in %s", pemFiles)
	}
	return multiVerifier(verifiers), nil
}

// pemReader is an iterator over PEM files
type pemReader struct {
	rest          []byte
	pass          string
	curr          *pem.Block
	err           error
	skipEncrypted bool
}

// newPemReader builds a new PEM reader
func newPemReader(file, password string, skipEncrypted bool) (*pemReader, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	return &pemReader{rest: data, pass: password, skipEncrypted: skipEncrypted}, nil
}

// Next returns true if there is some block to get
func (p *pemReader) Next() bool {
	if p.rest == nil || len(p.rest) <= 0 || p.err != nil {
		return false
	}
	block, rest := pem.Decode(p.rest)
	if block == nil {
		p.err = errors.New(InvalidData)
		return false
	}
	p.rest = rest
	if x509.IsEncryptedPEMBlock(block) {
		if !p.skipEncrypted {
			decrypted, err := x509.DecryptPEMBlock(block, []byte(p.pass))
			if err != nil {
				p.err = err
				return false
			}
			block.Bytes = decrypted
		}
	}
	p.curr = block
	return true
}

// Get returns the current PEM block
func (p *pemReader) Get() *pem.Block {
	return p.curr
}

// Error returns the last error
func (p *pemReader) Error() error {
	return p.err
}

type multiVerifier []Verifier

func (mv multiVerifier) Verify(hash []byte, signature string) error {
	if mv != nil && len(mv) > 0 {
		for _, v := range mv {
			if v.Verify(hash, signature) == nil {
				return nil
			}
		}
	}
	return errors.New(InvalidSignature)
}

type rsaVerifier struct {
	pkey *rsa.PublicKey
}

type ecVerifier struct {
	pkey *ecdsa.PublicKey
}

type ecdsaSignature struct {
	R, S *big.Int
}

// Verify implements Verifier interface
func (v rsaVerifier) Verify(hash []byte, signature string) error {
	s, err := hex.DecodeString(signature)
	if err != nil {
		return errors.Wrapf(err, "Failed to decode hex string %s", signature)
	}
	opts := CryptoOptions()
	if err := rsa.VerifyPSS(v.pkey, opts.HashFunc(), hash, s, nil); err != nil {
		return errors.Wrap(err, "Failed to verify token signature")
	}
	return nil
}

// Verify implements Verifier interface
func (v ecVerifier) Verify(hash []byte, signature string) error {
	s, err := hex.DecodeString(signature)
	if err != nil {
		return errors.Wrapf(err, "Failed to decode hex string %s", signature)
	}
	p := ecdsaSignature{}
	if _, err := asn1.Unmarshal(s, &p); err != nil {
		return errors.Wrap(err, "Failed to unmarshal signature")
	}
	if ecdsa.Verify(v.pkey, hash, p.R, p.S) == true {
		return nil
	}
	return errors.New(InvalidECSignature)
}
