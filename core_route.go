package saferest

import (
	"net/http"
	"os"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

// RBACFunc checks if a request is authorized, based on claims.
// Claims can be nil if the route does not enforce auth (Private = false)
//
// Can return:
// - Request, Error: if request is wrong.
// - Nil, Error: If request is unauthorized.
// - Request, Nil: If request is authorized.
//
// Thec checks implemented by this function are additional to Audience.
type RBACFunc func(r *http.Request, claims *jwt.StandardClaims) (*http.Request, int, error)

// NullRBAC allows any request. Useful when you want
// to allow requests even when Signer != Issuer
func NullRBAC(r *http.Request, claims *jwt.StandardClaims) (*http.Request, int, error) {
	return r, http.StatusOK, nil
}

// Route descriptor for the API
type Route struct {
	// Keyer finds out the proper secret to decode the token.
	// If nil, uses the global secret.
	Keyer Keyer
	// Audience identifier (for JWT authorization)
	Audience string
	// If this is nil, Subject must match Issuer.
	// Otherwise, the function is called and the request is rejected
	// if it returns an error or a nil http.Request pointer.
	RBAC RBACFunc
	// Method ("GET", "POST", etc)
	Method string
	// Route pattern
	Pattern string
	// True if token required
	Private bool
	// True to compress output
	Compress bool
	// True to cache output
	Cache bool
	// Handler function
	Handler http.HandlerFunc
}

// Bundle for all the route params together...
type routeParams struct {
	manager ConfigManager
	cache   stringCache
	router  *mux.Router
	prefix  string
}

// Appends a Route to the router
func addRoute(params *routeParams, route Route) {
	params.router.Methods(route.Method).
		Path(params.prefix + route.Pattern).
		//Name(route.Name).
		Handler(decorate(params, route))
}

// Decorates each Route with the required middleware.
// See https://auth0.com/blog/2016/04/13/authentication-in-golang/
func decorate(params *routeParams, route Route) http.Handler {
	serverConfig := GetServer(params.manager)
	handler := http.Handler(route.Handler)
	// Handlers must be added top-down, i.e. the first
	// handlers to apply to the request should be added last.
	// Add cache handler
	if route.Method == "GET" && route.Cache {
		maxAge := serverConfig.MaxAge
		handler = cacheContent(params.cache, maxAge, handler)
	}
	// Add compression handler. Before cache, so there is
	// less data to checksum.
	if route.Compress {
		handler = handlers.CompressHandler(handler)
	}
	// Add body size limiting for POST requests
	if route.Method == "POST" || route.Method == "PUT" {
		// convert MBytes to bytes
		bodyLimit := int64(serverConfig.MaxBodySize) << 20
		handler = limitBodySize(bodyLimit, handler)
	}
	// Add webtoken handler for private methods
	enforce := route.Private && (route.Audience != "")
	keyer := route.Keyer
	if keyer == nil {
		keyer = params
	}
	handler = checkToken(params.manager, keyer, enforce, route.RBAC, route.Audience, handler)
	// Add logging middleware to log every request
	handler = handlers.LoggingHandler(os.Stdout, handler)
	// Add metrics handler, to measure even logging delays
	handler = instrument(handler)
	// Finally add panic recovery, in case shit happens
	return handlers.RecoveryHandler(handlers.PrintRecoveryStack(true))(handler)
}

// KeyFunc returns a Key function that works for a single issuer.
// I put it here instead of the TokenConfig type so it can be dynamic, i.e.
// return the right token even if it is changed at runtime.
func (r *routeParams) KeyFunc(issuer string) (string, error) {
	config := GetToken(r.manager)
	if config.Secret == "" {
		return "", errors.New("Secret token is not configured")
	}
	if issuer != "" && issuer != config.Subject {
		return "", errors.Errorf("Issuer *%s* not found or does not match config", issuer)
	}
	return config.Secret, nil
}
