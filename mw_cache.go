package saferest

import (
	"encoding/hex"
	"fmt"
	"hash/fnv"
	"net/http"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// stringCache for Etags
type stringCache interface {
	// Add a key to the bucket belonging to the issuer
	Add(issuer, key, val string)
	// Get the key from the bucket belonging to the issuer. Returns the generation along with the value.
	Get(issuer, key string) (gen int64, val string, found bool)
	// Generation number assigned currently to the issuer
	Generation(issuer string) int64
}

var (
	promCacheHits *prometheus.CounterVec
	promCacheMiss *prometheus.CounterVec
)

func init() {
	promCacheHits = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "saferest",
		Subsystem: "cache",
		Name:      "weak_total",
		Help:      "Total number of cache hits with type (weak / strong etag)",
	}, []string{"issuer", "type"})
	promCacheMiss = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "saferest",
		Subsystem: "cache",
		Name:      "miss_total",
		Help:      "Total number of cache misses",
	}, []string{"issuer"})
}

// Middleware that caches responses.
// See https://justinas.org/writing-http-middleware-in-go/
func cacheContent(cache stringCache, maxAge int, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		issuer := IssuerOf(r)
		// Build a cache key from url and params
		cacheSum := fnv.New128a()
		cacheSum.Write([]byte(r.URL.RequestURI()))
		cacheKey := hex.EncodeToString(cacheSum.Sum(nil))
		// Generate the cache headers. Use a weak tag unless we have the value in the cache.
		var gen int64
		var hash, tag, weak string
		if cache != nil && cacheKey != "" {
			// Strong tags have both a generation number and a hash
			foundGen, foundEtag, ok := cache.Get(issuer, cacheKey)
			// The generation number for the issuer is always returned
			gen = foundGen
			// The hash, on the other hand, may or may not be found
			if ok {
				hash = foundEtag
				tag = fmt.Sprintf("\"%v:%s\"", gen, hash)
			}
		} else {
			// Ask only for the generation
			gen = cache.Generation(issuer)
		}
		weak = fmt.Sprintf("W/\"%v\"", gen)
		if tag == "" {
			tag = weak
		}
		addCacheHeaders(w, maxAge, tag)
		// Check to see if data is in cache
		hit := 0
		if rec := r.Header.Get("If-None-Match"); rec != "" {
			switch {
			// Weak tag for the same generation is always a match
			case rec == weak:
				hit = 1
			// Weak tags from different generations do not match
			case strings.HasPrefix(rec, "W/"):
				hit = 0
			// Strong tags in the same generation match too
			case strings.HasPrefix(rec, fmt.Sprintf("\"%v:", gen)):
				hit = 2
				// Strong tag for different generations match depending on hash
			case hash != "" && strings.HasSuffix(rec, fmt.Sprintf(":%s\"", hash)):
				hit = 2
			}
		}
		if hit > 0 {
			w.WriteHeader(http.StatusNotModified)
			t := "weak"
			if hit > 1 {
				t = "strong"
			}
			promCacheHits.WithLabelValues(issuer, t).Inc()
			return
		}
		// If there is no etag cache, just pass the request to next handler
		if cache == nil {
			next.ServeHTTP(w, r)
			return
		}
		promCacheMiss.WithLabelValues(issuer).Inc()
		// If there is a cache, compute the hash of the result
		hasher := newWriter(w)
		next.ServeHTTP(hasher, r)
		// Flush, in case we are zipping
		(http.Flusher)(hasher).Flush()
		// Save the hash for the next client.
		cache.Add(issuer, cacheKey, hex.EncodeToString(hasher.Sum(nil)))
	})
}

// Add Cache-friendly headers; see https://www.mnot.net/cache_docs/
func addCacheHeaders(w http.ResponseWriter, maxAge int, tag string) {
	w.Header().Set("Etag", tag)
	if maxAge > 0 {
		expires := time.Now().Add(time.Duration(maxAge) * time.Second)
		w.Header().Set("Cache-Control", fmt.Sprintf("max-age=%d; must-revalidate", maxAge))
		w.Header().Set("Expires", expires.UTC().Format(http.TimeFormat))
	}
}
