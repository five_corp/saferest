package saferest

import (
	"context"
	"encoding/base64"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

// HeaderSeparator is the character used as separator for fields in headers.
// Choose "|" as it is less prone to conflict with header values than ","
const HeaderSeparator = "|"

// Keyer is an interface for types with a KeyFunc function
type Keyer interface {
	// KeyFunc returns the secret to check the token
	KeyFunc(issuer string) (string, error)
}

// KeyFunc is a function that implements Keyer interface
type KeyFunc func(string) (string, error)

// KeyFunc implements Keyer interface
func (kf KeyFunc) KeyFunc(issuer string) (string, error) {
	return kf(issuer)
}

type issuerKey int

// unsignedIssuerOf returns the issuer FROM THE HEADER, or ""
// Being from the header, this issuer is not signed. Its only purpose is to find a key to check the token signature.
// Don't trust this issuer for anything else, use the one inside the claims.
func unsignedIssuerOf(token *jwt.Token) string {
	if token.Header != nil {
		if issuerObj, ok := token.Header["iss"]; ok {
			if issuer, ok := issuerObj.(string); ok {
				return issuer
			}
		}
	}
	return ""
}

// Signer is an object capable of producing a signature.
type Signer interface {
	Sign(audience string, expiration time.Duration) string
}

// SignerFunc is a helper to turn a function into a Signer
type SignerFunc func(audience string, expiration time.Duration) string

// Sign implements Signer interface
func (f SignerFunc) Sign(audience string, expiration time.Duration) string {
	return f(audience, expiration)
}

var signingMethod = jwt.SigningMethodHS512

// Sign creates a signed token
func Sign(secret, issuer, subject, audience string, expiration time.Duration) string {
	now := time.Now()
	claims := jwt.StandardClaims{
		Issuer:    issuer,
		Subject:   subject,
		ExpiresAt: now.Add(expiration).Unix(),
		// In case of clock skew
		IssuedAt:  now.Add(-60 * time.Second).Unix(),
		NotBefore: now.Add(-60 * time.Second).Unix(),
	}
	if audience != "" {
		claims.Audience = audience
	}
	token := jwt.NewWithClaims(signingMethod, claims)
	token.Header["iss"] = issuer
	//token.Header["sub"] = subject
	signed, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Print("Error signing token! ", err)
		return ""
	}
	return signed
}

// SplitAudience splits the claims audience in terms
func SplitAudience(claims *jwt.StandardClaims) []string {
	// Beware! Split an empty string returns a list with one item,
	// the empty string itself. Before splitting, check it is not empty.
	audience := strings.TrimSpace(claims.Audience)
	if audience == "" {
		return nil
	}
	return strings.Split(claims.Audience, HeaderSeparator)
}

// Get a token from Basic auth string
func tokenFromBasicAuth(basic string) string {
	// Decode user:pass from base 64.
	decoded, err := base64.StdEncoding.DecodeString(basic)
	if err != nil {
		log.Print("Attempted basic auth with error ", err)
		return ""
	}
	// Split on ":". First part is username, second part is token.
	// Username must be "debug".
	parts := strings.Split(string(decoded), ":")
	switch {
	case len(parts) < 1:
		log.Print("Attempted basic auth with no username or pass")
		return ""
	case len(parts) < 2:
		log.Print("Attempted basic auth without password, username ", parts[0])
		return ""
	case !strings.EqualFold(strings.TrimSpace(parts[0]), "debug"):
		log.Print("Attempted basic auth with wrong username ", parts[0])
		return ""
	}
	// Return the password part as a token
	return strings.TrimSpace(parts[1])
}

// Get the claims from a request
func getClaims(r *http.Request, sectioner Sectioner, keyer Keyer, enforce, rbac bool, audience string) (string, *jwt.StandardClaims, error) {
	config := GetToken(sectioner)
	keyFunc := func(token *jwt.Token) (interface{}, error) {
		key, err := keyer.KeyFunc(unsignedIssuerOf(token))
		if err != nil {
			return nil, err
		}
		return []byte(key), nil
	}
	// Accept both "X-access-token" and "Authorization: Bearer" as token vehicles
	header := r.Header.Get("X-access-token")
	if header == "" {
		header = r.Header.Get("Authorization")
		if header != "" {
			parts := strings.Split(header, " ")
			if len(parts) < 2 {
				header = ""
			} else {
				method := strings.TrimSpace(parts[0])
				switch {
				case strings.EqualFold(method, "bearer"):
					header = strings.TrimSpace(parts[1])
				case strings.EqualFold(method, "basic"):
					header = tokenFromBasicAuth(strings.TrimSpace(parts[1]))
				default:
					header = ""
				}
			}
		}
	}
	// If the token is not present, the request may go on if "enforce"==false.
	// If the token _is_ present, it must be valid despite of "enforce".
	if header == "" {
		if !enforce {
			log.Print("Missing token, authorization passed because enforce = false")
			return header, nil, nil
		}
		return header, nil, errors.New("Missing Authorization: Bearer header")
	}
	token, err := jwt.ParseWithClaims(header, &jwt.StandardClaims{}, keyFunc)
	if err != nil {
		return header, nil, errors.Wrap(err, "Failed to parse token claims")
	}
	claims, ok := token.Claims.(*jwt.StandardClaims)
	if !ok {
		return header, nil, errors.New("Only RFC7519#section-4.1 claims are recognized")
	}
	issuer := unsignedIssuerOf(token)
	switch {
	case !token.Valid:
		return header, nil, errors.New("The token is invalid or expired")
	case token.Method.Alg() != signingMethod.Alg():
		return header, nil, errors.New("The token method Algorithm is invalid, should be SHA512")
	case issuer != "" && issuer != claims.Issuer:
		return header, nil, errors.Errorf("Attempt to spoof issuer %s by %s", claims.Issuer, issuer)
	case issuer == "" && (claims.Issuer != config.Subject):
		return header, nil, errors.New("The token issuer does not match the configuration")
	case (!rbac && (claims.Issuer != claims.Subject)):
		return header, nil, errors.New("Operation only allowed for subject = issuer")
	case claims.Audience != "" && !checkAudience(claims, audience):
		return header, nil, errors.New("The method is not included in the token audience")
	}
	return header, claims, nil
}

// Middleware that checks the validity of the web token
func checkToken(sectioner Sectioner, keyer Keyer, enforce bool, rbac RBACFunc, audience string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Store the token in the request's context
		header, claims, err := getClaims(r, sectioner, keyer, enforce, (rbac != nil), audience)
		if err != nil {
			Fail(http.StatusUnauthorized, w, errors.Wrapf(err, "Invalid token '%s' in headers", header))
			return
		}
		// Add the issuer to the request
		issuer := ""
		if claims == nil {
			// If we have empty claims, it's because we are in debug mode, accepting requests without tokens.
			// In this case, use the default issuer as request issuer.
			issuer = GetToken(sectioner).Subject
		} else {
			issuer = claims.Issuer
		}
		r = r.WithContext(context.WithValue(r.Context(), issuerKey(0), issuer))
		// Context is available only to handlers down in the pipeline, not up.
		// Since metrics handler should be at the top of the hierarchy, to support
		// passing the issuer, we use the "dimensions" interface.
		dimensions(r)["issuer"] = issuer
		// RBAC must run even if claims == nil (not enforced), because
		// it may perform some additional tasks (like getting parameters
		// from GET or POST)
		if rbac != nil {
			newRequest, status, err := rbac(r, claims)
			// If err != nil, request is wrong
			if err != nil {
				Fail(status, w, err)
				return
			}
			// If request is fine, keep processing upon the
			// authorized request object.
			r = newRequest
		}
		// Go ahead with the next middleware.
		next.ServeHTTP(w, r)
	})
}

// Checks if methodAudience is included in tokenAudience
func checkAudience(claims *jwt.StandardClaims, methodAudience string) bool {
	for _, item := range SplitAudience(claims) {
		if methodAudience == strings.TrimSpace(item) {
			return true
		}
	}
	return false
}

// IssuerOf gets the issuer of the token used for a request
func IssuerOf(r *http.Request) string {
	result := ""
	if issuerVal := r.Context().Value(issuerKey(0)); issuerVal != nil {
		if issuer, ok := issuerVal.(string); ok {
			result = issuer
		}
	}
	return result
}
