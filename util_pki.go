package saferest

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"io"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

// GetRootCA loads the root CA specified by the config file
func GetRootCA(config CertConfig) (*x509.CertPool, error) {
	caContents, err := ioutil.ReadFile(config.RootFile)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to read Root CA file from %s", config.RootFile)
	}
	rootCerts := x509.NewCertPool()
	if !rootCerts.AppendCertsFromPEM(caContents) {
		return nil, errors.Wrap(err, "Failed to append root CA certificate")
	}
	log.Print("Loaded root CA certificate")
	return rootCerts, nil
}

// Enables TLS in a http.Server
func enableTLS(s *http.Server, c CertConfig, selfsign bool) error {
	// See https://blog.gopheracademy.com/advent-2016/exposing-go-on-the-internet/
	s.TLSConfig = &tls.Config{
		PreferServerCipherSuites: true,
		// Only use curves which have assembly implementations
		CurvePreferences: []tls.CurveID{
			tls.CurveP256,
			tls.X25519, // Go 1.8 only
		},
		MinVersion: tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305, // Go 1.8 only
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,   // Go 1.8 only
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			// Best disabled, as they don't provide Forward Secrecy,
			// but might be necessary for some clients
			// tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			// tls.TLS_RSA_WITH_AES_128_GCM_SHA256,
		},
	}
	if c.RootFile != "" {
		rootCerts, err := GetRootCA(c)
		if err != nil {
			return err
		}
		s.TLSConfig.RootCAs = rootCerts
		s.TLSConfig.ClientAuth = tls.NoClientCert
		s.TLSConfig.ClientCAs = rootCerts
	}
	if selfsign {
		cert, err := makeCert(rand.Reader, c.CertDomain, c.KeyBits)
		if err != nil {
			return err
		}
		s.TLSConfig.Certificates = []tls.Certificate{*cert}
	}
	return nil
}

/*
Generates a self-signed certificate with the given CN and RSA bits.
"random" is the source of entropy.

See https://golang.org/src/crypto/tls/generate_cert.go
*/
func makeCert(random io.Reader, cn string, bits int) (*tls.Certificate, error) {
	privKey, err := rsa.GenerateKey(random, bits)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to generate certificate signing Key")
	}
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to generate certificate serial number")
	}
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			CommonName: cn,
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(1, 0, 0),
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
		IsCA: true,
	}
	x509Cert, err := x509.CreateCertificate(random, &template, &template, privKey.Public(), privKey)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to self-sign certificate")
	}
	cert := &tls.Certificate{
		Certificate: [][]byte{
			x509Cert,
		},
		PrivateKey: privKey,
	}
	return cert, nil
}
