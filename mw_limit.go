package saferest

import "net/http"

// Middleware that limits the size of request bodies
func limitBodySize(maxBytes int64, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Replace original body with a bounded one.
		// MaxBytesReader is a ReadCloser, it will close the
		// original body when finished.
		r.Body = http.MaxBytesReader(w, r.Body, maxBytes)
		next.ServeHTTP(w, r)
	})
}
