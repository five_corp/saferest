package saferest

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// type used to store request start time in r.Context()
type timeKey int

// Size of each chunk in a chunked response
const flushThreshold = 65535

var (
	promRequests      *prometheus.CounterVec
	promResponseBytes *prometheus.CounterVec
	promFirstByteTime *prometheus.HistogramVec
	promResponseTime  *prometheus.HistogramVec
)

func init() {
	dimensions := []string{"method", "url", "issuer", "status"}
	promRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "saferest",
		Subsystem: "http",
		Name:      "requests_total",
		Help:      "Total number of http requests",
	}, dimensions)
	promResponseBytes = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "saferest",
		Subsystem: "http",
		Name:      "response_bytes",
		Help:      "Total size of http responses",
	}, dimensions)
	promFirstByteTime = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "saferest",
		Subsystem: "http",
		Name:      "firstbyte_seconds",
		Help:      "Total time to first byte of response in seconds",
	}, dimensions)
	promResponseTime = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "saferest",
		Subsystem: "http",
		Name:      "response_seconds",
		Help:      "Total time to response in seconds",
	}, dimensions)
}

// Middleware that limits the size of request bodies
func instrument(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		now := time.Now()
		dim := map[string]string{
			"method": r.Method,
			"url":    r.URL.Path,
		}
		out := &flushWriter{
			ResponseWriter: w,
			threshold:      flushThreshold,
		}
		// Make both the start time and dimensions array to other handlers
		ctx := context.WithValue(r.Context(), timeKey(0), now)
		ctx = context.WithValue(ctx, timeKey(1), dim)
		r = r.WithContext(ctx)
		// Get parameters *after* the request is served, because some things (like the issuer)
		// will not be available until a handler later on updates the request context.
		next.ServeHTTP(out, r)
		end, firstByte := time.Now(), out.firstByte
		if firstByte.IsZero() {
			firstByte = end
		}
		dims := []string{
			dim["method"],
			dim["url"],
			dim["issuer"],
			strconv.Itoa(out.statusCode),
		}
		promRequests.WithLabelValues(dims...).Inc()
		promResponseBytes.WithLabelValues(dims...).Add(float64(out.totalBytes))
		promFirstByteTime.WithLabelValues(dims...).Observe(float64(firstByte.Sub(now) / time.Second))
		promResponseTime.WithLabelValues(dims...).Observe(float64(end.Sub(now) / time.Second))
	})
}

// StartTime returns the start time of the request
func StartTime(r *http.Request) time.Time {
	return r.Context().Value(timeKey(0)).(time.Time)
}

// flushWriter is a writer that flushes every given amount of bytes
type flushWriter struct {
	http.ResponseWriter
	statusCode int
	firstByte  time.Time
	totalBytes int
	bytes      int
	threshold  int
}

// Write implements io.Writer
func (f *flushWriter) Write(p []byte) (int, error) {
	n, err := f.ResponseWriter.Write(p)
	if err == nil {
		f.totalBytes += n
		f.bytes += n
		if f.bytes > f.threshold {
			if flusher, ok := f.ResponseWriter.(http.Flusher); ok {
				//log.Print("VERBOSE: Response too long, flushing for chunked encoding")
				flusher.Flush()
				if f.firstByte.IsZero() {
					f.firstByte = time.Now()
				}
			}
			f.bytes = 0
		}
	}
	return n, err
}

// WriteHeader implements http.ResponseWriter interface
func (f *flushWriter) WriteHeader(statusCode int) {
	f.statusCode = statusCode
	f.ResponseWriter.WriteHeader(statusCode)
}

// Dimensions enumerates dimensions in the proper order to update a metric
func dimensions(r *http.Request) map[string]string {
	return r.Context().Value(timeKey(1)).(map[string]string)
}
