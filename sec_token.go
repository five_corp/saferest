package saferest

import (
	"io"
	"time"
)

// SectionToken is the name of the config section for tokens
const SectionToken = "token"

// TokenConfig configuration parameters
type TokenConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// Token secret and subject for JWT signing
	// These are not serialized here.
	Secret  string `json:"secret" xml:"secret" yaml:"secret"`
	Subject string `json:"subject" xml:"subject" yaml:"subject"`
}

// NewTokenConfig adds the CertConfig to a ConfigMa ager
func newTokenConfig(parser Parser, input io.Reader) Config {
	// Set sane default values for numeric parameters
	config := TokenConfig{
		Version: 1,
		Secret:  "",
		Subject: "",
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetToken retrieves token config
func GetToken(manager Sectioner) TokenConfig {
	return manager.Section(SectionToken).(TokenConfig)
}

// Sign implements Signer interface
func (config TokenConfig) Sign(audience string, expiration time.Duration) string {
	return Sign(config.Secret, config.Subject, config.Subject, audience, expiration)
}

// WithSubject returns a Signer with the given subject
func (config TokenConfig) WithSubject(issuer, subject string) Signer {
	return SignerFunc(func(audience string, expiration time.Duration) string {
		return Sign(config.Secret, issuer, subject, audience, expiration)
	})
}
