package saferest

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// Response defines the Normalized response object returned by the API
type Response struct {
	Success bool        `json:"success"`
	Message interface{} `json:"message"`
	Plain   bool        `json:"-"`
}

// MaxBodySize is the maximum size of POST, PUT body (json encoded)
const MaxBodySize = 1024 // KBytes

// Streamer objects write themselves to the http.ResponseWriter, in JSON format
type Streamer interface {
	Stream(w io.Writer) error
}

//  Dispatch the response object to the given writer
func (r *Response) dispatch(code int, w http.ResponseWriter) {
	// Let's see if we should compress the data
	now := time.Now()
	w.Header().Set("Date", now.UTC().Format(http.TimeFormat))
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Connection", "keep-alive")
	// Allows chunked responses
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	if streamer, ok := r.Message.(Streamer); ok {
		// Write to the flusher
		if !r.Plain {
			w.Write([]byte("{ \"success\": true, \"message\": "))
		}
		if err := streamer.Stream(w); err != nil {
			log.Print("Failed to stream response! ", err)
		}
		if !r.Plain {
			w.Write([]byte("}"))
		}
	} else {
		resp := interface{}(r)
		if r.Plain {
			resp = r.Message
		}
		if err := json.NewEncoder(w).Encode(resp); err != nil {
			log.Print("Failed to encode response! ", r, ", err: ", err)
		}
	}
}

// Fail sends the message to the remote end, and logs the error
func Fail(code int, w http.ResponseWriter, err error) {
	response := Response{Success: false, Message: err.Error()}
	if code == http.StatusUnauthorized {
		w.Header().Set("WWW-Authenticate", "Basic realm=\"saferest-API\"")
	}
	response.dispatch(code, w)
	if err != nil {
		log.Printf("Error: %+v", err)
	}
}

// Success sends a 200 OK message.
// The message is embedded in a JSON Response; if message implements
// the Streamer interface, the Stream(io.Writer) method is called to
// dump the object (in json format) to the writer.
func Success(w http.ResponseWriter, message interface{}) {
	response := Response{Success: true, Message: message}
	response.dispatch(http.StatusOK, w)
}

// SuccessPlain sends a 200 OK message without embedding in any struct
func SuccessPlain(w http.ResponseWriter, message interface{}) {
	response := Response{Success: true, Message: message, Plain: true}
	response.dispatch(http.StatusOK, w)
}

// GetJSONBody reads the JSON / YAML body of a request into the given data object
func GetJSONBody(h http.Header, r io.Reader, data interface{}) error {
	contentType, isYaml := h.Get("Content-Type"), false
	if !strings.Contains(contentType, "application/json") {
		// No standard MIME type for yaml, see
		// http://stackoverflow.com/questions/332129/yaml-mime-type
		if !strings.Contains(contentType, "application/x-yaml") {
			return errors.Errorf("Invalid content-type %s", contentType)
		}
		isYaml = true
	}
	maxSize := int(MaxBodySize<<10) + 1
	body, err := ioutil.ReadAll(io.LimitReader(r, int64(maxSize)))
	if err != nil {
		return errors.Wrap(err, "Failed to red request body")
	}
	if len(body) >= maxSize {
		return errors.Errorf("Size of request is too large. Max Size: ", maxSize-1)
	}
	if isYaml {
		body, err = yamlToJSON(body)
		if err != nil {
			return err
		}
	}
	if err := json.Unmarshal(body, data); err != nil {
		return errors.Wrapf(err, "Failed to decode request body to type %T", data)
	}
	return nil
}

// RestRequest sends a request to a REST server. Parameters:
// - method: "POST", "GET", etc.
// - url: URL to submit the request to
// - token: security token, may be nil for unsafe requests
// - timeout: seconds to wait for the answer
// - body: request body, may be nil (struct)
// - skipVerify: True to skip SSL verification
// - result: result expected from Home (address of struct)
func RestRequest(ctx context.Context, method, url, token string, timeout int, body interface{}, skipVerify bool, result interface{}) error {
	// Serialize the body, if not nil
	var bodyReader io.Reader
	if body != nil {
		data, err := json.Marshal(body)
		if err != nil {
			return errors.Wrap(err, "Failed to marshal REST request")
		}
		bodyReader = bytes.NewReader(data)
	}
	// Build the request
	req, err := http.NewRequest(method, url, bodyReader)
	if err != nil {
		return errors.Wrapf(err, "Failed to build request for %s", url)
	}
	if ctx != nil {
		req = req.WithContext(ctx)
	}
	// Appeal CORS checks
	req.Header.Add("Origin", req.URL.Scheme+"://"+req.URL.Host)
	// Add security token, if needed
	if token != "" {
		req.Header.Add("X-access-token", token)
	}
	// Send request, expect json
	if body != nil {
		// This is required, otherwise no body is sent...
		req.Header.Add("Content-Type", "application/json")
	}
	req.Header.Add("Accept", "application/json")
	client := http.Client{
		Timeout: time.Duration(timeout) * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: skipVerify},
		},
	}
	resp, err := client.Do(req)
	// Resp may be != nil even if err != nil
	// See http://devs.cloudimmunity.com/gotchas-and-common-mistakes-in-go-golang/
	if resp != nil && resp.Body != nil {
		defer (func(body io.ReadCloser) {
			io.Copy(ioutil.Discard, body)
			body.Close()
		})(resp.Body)
	}
	if err != nil {
		return errors.Wrapf(err, "Failed to perform REST request to %s", url)
	}
	// Check status code
	if resp.StatusCode != 200 {
		body, err := ioutil.ReadAll(io.LimitReader(resp.Body, MaxBodySize<<10))
		if err != nil {
			return errors.Wrapf(err, "Failed to read REST response body from %s", url)
		}
		return errors.Errorf("Rejected by Home url %s, data: %s", url, string(body))
	}
	// Check body content
	if err := GetJSONBody(resp.Header, resp.Body, result); err != nil {
		return err
	}
	return nil
}

// Emulates a Response struct in the receiver side
type wrappedResponse struct {
	Success bool            `json:"success"`
	Message json.RawMessage `json:"message"`
}

// RestRequestWrapped is like RestRequest, but expects the response to be wrapped in a Response Object
func RestRequestWrapped(ctx context.Context, method, url, token string, timeout int, body interface{}, skipVerify bool, result interface{}) error {
	wrappedResult := wrappedResponse{}
	if err := RestRequest(ctx, method, url, token, timeout, body, skipVerify, &wrappedResult); err != nil {
		return err
	}
	if !wrappedResult.Success {
		var message string
		if err := json.Unmarshal(wrappedResult.Message, &message); err != nil {
			return errors.Wrapf(err, "Failed to unmarshal REST reply from %s", url)
		}
		return errors.Errorf("REST request error: %s", message)
	}
	if err := json.Unmarshal(wrappedResult.Message, result); err != nil {
		return errors.Wrapf(err, "Failed to unmarshal reply from %s", url)
	}
	return nil
}
