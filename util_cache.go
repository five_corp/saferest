package saferest

import (
	"log"
	"sync"
	"time"

	lru "github.com/hashicorp/golang-lru"
)

// Alias for my selected cache engine
type lruCache struct {
	cacheSize int
	sync.RWMutex
	buckets    map[string]*lruBucket
	generation int64
}

// Each entry of the lruCache map
type lruBucket struct {
	arc        *lru.ARCCache
	generation int64
}

func (cache *lruCache) init(size int) {
	cache.cacheSize = size
	cache.generation = time.Now().Unix()
	cache.buckets = make(map[string]*lruBucket)
}

// Add an etag to the cache
func (cache *lruCache) Add(issuer, hash, etag string) {
	cache.Lock()
	var arc *lru.ARCCache
	if bucket, ok := cache.buckets[issuer]; ok {
		arc = bucket.arc
		cache.Unlock()
		// Silently discard cached value if cache not ready
		if arc == nil {
			return
		}
	} else { // It is my duty to create the cache
		bucket = &lruBucket{generation: cache.generation, arc: nil}
		cache.buckets[issuer] = bucket
		var err error
		arc, err = lru.NewARC(cache.cacheSize)
		if err != nil {
			log.Print("Warning: Could not create etag cache for", issuer, ", error:", err)
			cache.Unlock()
			return
		}
		bucket.arc = arc
		cache.Unlock()
	}
	arc.Add(hash, etag)
}

// Gets an etag from the cache
func (cache *lruCache) Get(issuer, hash string) (gen int64, tag string, found bool) {
	cache.RLock()
	gen = cache.generation
	var arc *lru.ARCCache
	if bucket, ok := cache.buckets[issuer]; ok {
		arc = bucket.arc
		gen = bucket.generation
	}
	cache.RUnlock()
	if arc != nil {
		if obj, ok := arc.Get(hash); ok {
			tag, found = obj.(string)
		}
	}
	return
}

// Purges the cache
func (cache *lruCache) Purge(issuer string) error {
	generation := time.Now().Unix()
	var arc *lru.ARCCache
	cache.Lock()
	if bucket, ok := cache.buckets[issuer]; ok {
		arc = bucket.arc
		bucket.generation = generation
	}
	cache.generation = generation
	cache.Unlock()
	if arc != nil {
		arc.Purge()
	}
	return nil
}

// Generation currently in place
func (cache *lruCache) Generation(issuer string) int64 {
	cache.RLock()
	generation := cache.generation
	if bucket, ok := cache.buckets[issuer]; ok {
		generation = bucket.generation
	}
	cache.RUnlock()
	return generation
}
